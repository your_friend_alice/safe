#!/bin/bash

set -ex

NAME="safe"
DESCRIPTION="Add an extra confirmation prompt before running a command."
MAINTAINER="alice@dn3s.me"
VERSION="${1:-$CI_COMMIT_TAG}"
CLEANED_VERSION="$(echo "$VERSION" | sed -e 's/^v//g')"
export PUSH_RELEASE_ASSET_LINK_TOKEN
echo "PUSH_RELEASE_ASSET_LINK_TOKEN: $PUSH_RELEASE_ASSET_LINK_TOKEN"

[[ -z "$VERSION" ]] && echo "version arg (or \$CI_COMMIT_TAG) must be set!" && exit 1

mkdir -p out
createRelease() {
    if [[ $CI == "true" ]]; then
        echo "Creating release $CI_COMMIT_TAG"
        status="$(CI_COMMIT_TAG=$CI_COMMIT_TAG jq -n '{name: env.CI_COMMIT_TAG, tag_name: env.CI_COMMIT_TAG}' | \
            curl --silent -w '%{http_code}' -o /dev/stderr \
                --header "JOB-TOKEN: $CI_JOB_TOKEN" \
                --header "Content-Type: application/json" \
                -XPOST --data '@-' \
                "$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases" \
            )"
        [[ $status != 20* ]] && exit 1 || true
        echo
    fi
}
addReleaseLink() {
    echo "Adding asset $1 to release $2"
    status="$(curl --silent -w '%{http_code}' -o /dev/stderr \
        --header "PRIVATE-TOKEN: $PUSH_RELEASE_ASSET_LINK_TOKEN" \
        -XPOST \
        --data "link_type=package" \
        --data "name=$(basename "$1")" \
        --data "url=$1" \
        "$CI_API_V4_URL/projects/$CI_PROJECT_ID/releases/$CI_COMMIT_TAG/assets/links" \
        )"
    [[ $status != 201 ]] && exit 1 || true
    echo
}
pushPackage() {
    if [[ $CI == "true" ]]; then
        echo "Pushing package $1"
        PACKAGE_URL="$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/$NAME/$CLEANED_VERSION/$(basename "$1")"
        status="$(curl --silent -w '%{http_code}' -o /dev/stderr \
            --header "JOB-TOKEN: $CI_JOB_TOKEN" \
            --upload-file "$1" \
            "$PACKAGE_URL" \
            )"
        [[ $status != 201 ]] && exit 1 || true
        echo
        addReleaseLink "$PACKAGE_URL"
    fi
}

buildDeb() {
    SRCDIR="$1"
    PKGNAME="$2"
    GOARCH="$3"
    DEB_DIR="$(mktemp -d deb.XXXXXX)"
    mkdir -p "$DEB_DIR/usr/bin"
    cp "$1" "$DEB_DIR/usr/bin"
    mkdir -p "$DEB_DIR/DEBIAN"
    cat <<EOF > "$DEB_DIR/DEBIAN/control"
Package: $NAME
Version: $CLEANED_VERSION
Architecture: $GOARCH
Maintainer: $MAINTAINER
Description: $DESCRIPTION
EOF
    dpkg-deb --root-owner-group --build "$DEB_DIR" "$PKGNAME"
    rm -r "$DEB_DIR"
}

createRelease
for GOARCH in "386" "amd64" "arm" "arm64"; do
    for GOOS in "linux" "darwin"; do
        [[ "$GOOS" == darwin ]] && [[ "$GOARCH" != amd64 ]] && [[ "$GOARCH" != arm64 ]] && continue
        export GOOS GOARCH
        DIR="out/$CLEANED_VERSION.$GOOS.$GOARCH"
        PKGNAME="out/$NAME.$CLEANED_VERSION.$GOOS.$GOARCH.tar.gz"
        echo "Building $PKGNAME"
        rm -rf "$DIR"
        mkdir -p "$DIR/bin"
        CGO_ENABLED=0 go build -ldflags "-w -s" -o "$DIR/bin/$NAME"
        tar -cz "$DIR" | gzip --best > "$PKGNAME"
        pushPackage "$PKGNAME"
        if [[ "$GOOS" == linux ]]; then
            PKGNAME="out/$NAME.$CLEANED_VERSION.$GOOS.$GOARCH.deb"
            echo "Building $PKGNAME"
            buildDeb "$DIR/bin/safe" "$PKGNAME" "$GOARCH"
            pushPackage "$PKGNAME"
        fi
        rm -rf "$DIR"
    done
done
