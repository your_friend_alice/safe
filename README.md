# safe

"Safe" your commands. It's like sudo, but with no passwords or privilege escalation! Just type safe before typing a dangerous command, to protect you from accidentally hitting enter. Use this when you need to shake hands with danger.

![shake hands with danger](shake-hands-with-danger.mp3)

## Usage

```sh
% safe rm -rf $HOME/$SOME_VAR
About to run this command:
- "rm"
- "-rf"
- "/home/alice/"
To proceed, type "Yes" exactly.
> HECK NO!
User did not type "Yes", exiting.
```
