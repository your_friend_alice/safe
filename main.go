package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"os/exec"
)

const Answer = "Yes"

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("Usage: %s <command>\n", os.Args[0])
		os.Exit(1)
	}
	if verified, err := verify(os.Args[1:]); err != nil {
		fmt.Printf("Error prompting user: %s\n", err)
		os.Exit(1)
	} else if !verified {
		fmt.Printf("User did not type %q, exiting.\n", Answer)
		os.Exit(1)
	}
	run(os.Args[1:])
}

func verify(cmd []string) (bool, error) {
	fmt.Println("About to run this command:")
	for _, arg := range cmd {
		fmt.Printf("- %q\n", arg)
	}
	fmt.Printf("To proceed, type %q exactly.\n> ", Answer)
	result, err := ttyPrompt()
	if err != nil {
		return false, err
	}
	return result == Answer+"\n", nil
}

func ttyPrompt() (string, error) {
	f, err := os.Open("/dev/tty")
	if err != nil {
		return "", err
	}
	defer f.Close()
	reader := bufio.NewReader(f)
	return reader.ReadString('\n')
}

func run(cmd []string) {
	var c *exec.Cmd
	if len(cmd) == 1 {
		c = exec.Command(cmd[0])
	} else {
		c = exec.Command(cmd[0], cmd[1:]...)
	}
	c.Stdin = os.Stdin
	c.Stdout = os.Stdout
	c.Stderr = os.Stderr
	err := c.Run()
	var exitErr *exec.ExitError
	if err != nil && errors.As(err, &exitErr) {
		os.Exit(exitErr.ExitCode())
	} else if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
